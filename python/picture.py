import numpy as np
import cv2
import os
import shutil
import json
import math
from ftplib import FTP
from socketIO_client import SocketIO

filename = 'current.png'
path_to_this_script = os.path.dirname(__file__)
image_path = os.path.abspath(os.path.join(path_to_this_script, "../node/public", filename))
cam_width = 1280
cam_height = 720
cam_brightness = 0.7
cam_contrast = 0.15
cam_saturation = 0.15
cam_gain = 0 # no difference noticeable
compression_params = list()
compression_params.append(16) # CV_IMWRITE_PNG_COMPRESSION = 16
compression_params.append(3)

socketIO = SocketIO('localhost', 3000)

class Seat():
	def __init__(self, id, color, position, occupation):
		self.id = id
		self.color = color
		self.position = position
		self.occupation = occupation

	def setOccupation(self, occupation):
		self.occupation = occupation

class Color():
	def __init__(self, r, g, b):
		self.r = r
		self.g = g
		self.b = b

	def __str__(self):
		return str([self.r, self.g, self.b])

class Position():
	def __init__(self, x, y, w, h):
		self.x = x
		self.y = y
		self.w = w
		self.h = h

seats = []
seats_initialized = False
tolerance = 75

def init_seats(seats_data):
	for i in range(0, len(seats_data)):
		seat_data = json.dumps(seats_data[i])
		id = json.loads(seat_data)['id']
		color = json.loads(seat_data)['color']
		position = json.loads(seat_data)['position']
		occupation = json.loads(seat_data)['occupation']
		seats.append(Seat(id, Color(color['r'], color['g'], color['b']), Position(position['x'], position['y'], position['w'], position['h']), occupation))
	if len(seats) > 0:
		global seats_initialized
		seats_initialized = True
	
def clear_folder():
	for the_file in os.listdir(folder):
		file_path = os.path.join(folder, the_file)
		try:
			if os.path.isfile(file_path):
				os.unlink(file_path)
		except Exception as e:
			print(e)

def init_camera():
	cap = cv2.VideoCapture(0)
	cap.set(3, cam_width) # CV_CAP_PROP_FRAME_WIDTH = 3
	cap.set(4, cam_height) # CV_CAP_PROP_FRAME_HEIGHT = 4
	cap.set(10, cam_brightness) # CV_CAP_PROP_BRIGHTNESS = 10
	cap.set(11, cam_contrast) # CV_CAP_PROP_CONTRAST = 11
	cap.set(12, cam_saturation) # CV_CAP_PROP_SATURATION = 12
	cap.set(14, cam_gain) # CV_CAP_PROP_GAIN = 14
	return cap

def take_picture(data):
	for i in range(0, 6):
		image = cap.read()[1]
	
	print("took image: " + image_path)

	# store image to file
	cv2.imwrite(image_path, image, compression_params)

	# upload image to ftp
	session = FTP('213.133.104.162', 'internpfg_0', 'QCc5t1MSLk3EpdLZ')
	file = open(image_path, 'rb')
	session.storbinary('STOR current.png', file)
	file.close()
	session.quit()

	socketIO.emit('pictureTaken')

def check_for_occupation():
	seats_to_change = []

	for i in range(0, 6):
		image = cap.read()[1]

	for i in range(0, len(seats)):
		position = seats[i].position
		x = position.x
		y = position.y
		w = position.w
		h = position.h
		color = seats[i].color

		# numpy arrays are height x width
		area = image[y:y+h, x:x+w]

		# average per row, average per column, round to int, reverse
		current_color = np.around(np.average(np.average(area, axis=1), axis=0))[::-1]
		deviation = math.fabs(current_color[0] - color.r) + math.fabs(current_color[1] - color.g) + math.fabs(current_color[2] - color.b);

		if deviation > tolerance and not seats[i].occupation:
			seats[i].setOccupation(True)
			seats_to_change.append(i)
		elif deviation < tolerance and seats[i].occupation:
			seats[i].setOccupation(False)
			seats_to_change.append(i)

	json_object = ''

	for i in range(0, len(seats_to_change)):
		json_object = json_object + '{"id": ' + str(seats[seats_to_change[i]].id) + ', "o": ' + str(int(seats[seats_to_change[i]].occupation == True)) + '}'
		if i != len(seats_to_change) - 1:
			json_object = json_object + ', '


	if len(seats_to_change) > 0:
		json_object = '{"s": [ ' + json_object + ' ]}'
		socketIO.emit('seatsChanged', json_object)

cap = init_camera()

while True:
	socketIO.on('takePicture', take_picture)
	socketIO.on('seatDataSaved', init_seats)
	socketIO.wait(seconds=1)

	if seats_initialized:
		check_for_occupation()