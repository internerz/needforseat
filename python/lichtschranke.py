import RPi.GPIO as GPIO
from socketIO_client import SocketIO

inner_led_pin = 7
outer_led_pin = 13
GPIO.setmode(GPIO.BOARD)
GPIO.setup(inner_led_pin, GPIO.IN)
GPIO.setup(outer_led_pin, GPIO.IN)

socketIO = SocketIO('localhost', 3000)

count = 0
state = "start"
last_count = -1

while True:
    inner_led = GPIO.input(inner_led_pin)
    outer_led = GPIO.input(outer_led_pin)
    last_count = count

    if state == "start":
        if outer_led == 0:
            if inner_led == 0:
                state = "start"
            else:
                state = "out1"
        else:
            if inner_led == 0:
                state = "in1"
            else:
                state = "start"
            
    elif state == "out1":
        if outer_led == 0:
            if inner_led == 0:
                state = "start"
            else:
                state = "out1"
        else:
            if inner_led == 0:
                state = "out3"
            else:
                state = "out2"

    elif state == "out2":
        if outer_led == 0:
            if inner_led == 0:
                state = "start"
            else:
                state = "out1"
        else:
            if inner_led == 0:
                state = "out3"
            else:
                state = "out2"

    elif state == "out3":
        if outer_led == 0:
            if inner_led == 0:
                state = "start"
                count = count - 1
            else:
                state = "out1"
        else:
            if inner_led == 0:
                state = "out3"
            else:
                state = "out2"

    elif state == "in1":
        if outer_led == 0:
            if inner_led == 0:
                state = "start"
            else:
                state = "in3"
        else:
            if inner_led == 0:
                state = "in1"
            else:
                state = "in2"

    elif state == "in2":
        if outer_led == 0:
            if inner_led == 0:
                state = "start"
            else:
                state = "in3"
        else:
            if inner_led == 0:
                state = "in1"
            else:
                state = "in2"

    elif state == "in3":
        if outer_led == 0:
            if inner_led == 0:
                state = "start"
                count = count + 1
            else:
                state = "in3"
        else:
            if inner_led == 0:
                state = "in1"
            else:
                state = "in2"

    else:
        state = "start"

    if count < 0:
        count = 0


    if count != last_count:
        socketIO.emit('counterChange', str(count))
        print count