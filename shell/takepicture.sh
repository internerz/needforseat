#!/bin/bash
cd /home/pi/pictures/

# deletes all but the last 2 files 
#ls -t | tail -n +3 | xargs rm --

for i in 1 2 3
do
	fswebcam -r 1280x720 -q /home/pi/pictures/temp_current.jpg

	# removes last picture
	if [ -f last.jpg ]; then
		rm last.jpg
	fi

	# renames last picture
	if [ -f current.jpg ]; then
		mv current.jpg last.jpg
	fi

	# renames last picture
	mv temp_current.jpg current.jpg

	sleep 10
done

