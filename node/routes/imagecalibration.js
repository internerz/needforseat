var express = require('express');
var router = express.Router();

router.get('/imagecalibration', function(req, res, next) {
  res.render('imagecalibration', { title: 'Image Calibration' });
});

module.exports = router;
