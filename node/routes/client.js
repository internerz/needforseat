var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('client', { title: 'NeedForSeat', bodyClass: 'app station' });
});

module.exports = router;
