var express = require('express');
var router = express.Router();

router.get('/prototype', function(req, res, next) {
  res.render('prototype', { title: 'NeedForSeat Prototyp' });
});

module.exports = router;