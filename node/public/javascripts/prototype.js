(function ($) {
    $(function () {
        var socket = io.connect();

        $('.seats input').change(function() {
            socket.emit("seatsChange", {"seat_id": $(this).val(), "occupied": $(this).is(':checked')});
        });

        socket.on("seatsChange", function(data) {
            $('#seat' + data.seat_id).prop('checked', data.occupied);
        });

        socket.on("seatsInit", function(data) {
            $.each(data, function(key, seat) {
                $('#seat' + seat.id).prop('checked', seat.occupation);
            });
        });
    });
})(jQuery);