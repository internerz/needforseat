(function ($) {
    $(function () {
        var socket = io.connect();

        socket.emit("client", {"isClient": true});

        socket.on("changeSeats", function(data) {
            data = JSON.parse(data);
            $.each(data, function(key, seat) {
                $('#seat' + seat.id).prop('checked', seat.occupation);
            });
        });

        socket.on("seatsInit", function(data) {
            $.each(data, function(key, seat) {
                $('#seat' + seat.id).prop('checked', seat.occupation);
            });
        });

        socket.on("counterChange", function(count) {
            $('#counter').text(count);
        });
    });
})(jQuery);