(function ($) {
    $(function () {
        var socket = io.connect();
        var deleteActive = false;
        var image = document.getElementById('image');
        var ctx = image.getContext('2d');
        drawImageOnCanvas();

        var buttons = $('#buttons');
        var takePictureButton = $('#takepicture');
        var saveButton = $('#save');
        var recalibrateButton = $('#recalibrate');
        var deleteButton = $('#delete');
        var canvasElement = $('#canvas');
        var areas = [];

        initDraw(document.getElementById('canvas'));

        saveButton.click(function() {
            var areasElement = document.getElementsByClassName('area');

            var color;

            [].forEach.call(areasElement, function(a) {
                var area = {
                    'id': "",
                    'x': 0,
                    'y': 0,
                    'w': 0,
                    'h': 0,
                    'color': {'r': 0, 'g': 0, 'b': 0}
                };

                area.id = parseInt(a.getElementsByClassName("id")[0].innerHTML.split("#")[1]);
                area.x = parseInt(a.style.left.replace('px', ''));
                area.y = parseInt(a.style.top.replace('px', ''));
                area.w = parseInt(a.style.width.replace('px', ''));
                area.h = parseInt(a.style.height.replace('px', ''));
                color = a.style.backgroundColor.substring(4, a.style.backgroundColor.length-1).replace(/ /g, '').split(',');
                area.color.r = color[0] + '';
                area.color.g = color[1] + '';
                area.color.b = color[2] + '';

                areas.push(area);

                recalibrateButton.show();
            });

            $(this).addClass('loading');
            toggleButtonActivationStatus();

            setTimeout(function () {
                saveButton.removeClass('loading');
                toggleButtonActivationStatus();
            }, 1000);

            socket.emit("seatsSaved", JSON.stringify(areas));
        });

        takePictureButton.click(function () {
            $(this).addClass('loading');
            toggleButtonActivationStatus();
            socket.emit("takePicture", true);
        });

        recalibrateButton.click(function() {
            $.each(areas, function(k, area) {
                var rgb = getAreaAverageColor(parseInt(area.x), parseInt(area.y), parseInt(area.w), parseInt(area.h));
                $('#area' + area.id).css('background-color', 'rgba(' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ', 1)');
            });
        });

        socket.on("pictureTaken", function() {
            drawImageOnCanvas();
            takePictureButton.removeClass('loading');
            toggleButtonActivationStatus();
        });

        deleteButton.click(function() {
            if (deleteButton.hasClass('loading')) {
                toggleButtonActivationStatus();
                canvasElement.removeClass('delete');
                deleteButton.removeClass('loading');
                deleteActive = false;
            } else {
                toggleButtonActivationStatus();
                canvasElement.addClass('delete');
                deleteButton.addClass('loading');
                deleteActive = true;
            }
        });

        function toggleButtonActivationStatus() {
            buttons.toggleClass('deactivated');
        }

        function initDraw(canvas) {
            function setMousePosition(e) {
                var ev = e || window.event; //Moz || IE
                if (ev.pageX) { //Moz
                    mouse.x = ev.pageX + window.pageXOffset - canvasOffset.left;
                    mouse.y = ev.pageY + window.pageYOffset - canvasOffset.top;
                } else if (ev.clientX) { //IE
                    mouse.x = ev.clientX + document.body.scrollLeft;
                    mouse.y = ev.clientY + document.body.scrollTop;
                }
            }

            var canvasOffset = canvas.getBoundingClientRect();
            var idCounter = 0;

            var mouse = {
                x: 0,
                y: 0,
                startX: 0,
                startY: 0
            };

            var areaElement = null;
            var idElement = null;

            canvas.onmousemove = function (e) {
                setMousePosition(e);
                if (areaElement !== null) {
                    areaElement.style.width = Math.abs(mouse.x - mouse.startX) + 'px';
                    areaElement.style.height = Math.abs(mouse.y - mouse.startY) + 'px';
                    areaElement.style.left = (mouse.x - mouse.startX < 0) ? mouse.x + 'px' : mouse.startX + 'px';
                    areaElement.style.top = (mouse.y - mouse.startY < 0) ? mouse.y + 'px' : mouse.startY + 'px';
                }
            };

            canvas.onclick = function() {
                if (areaElement !== null && !deleteActive) {
                    var rgb = getAreaAverageColor(areaElement.style.left, areaElement.style.top, areaElement.style.width, areaElement.style.height);
                    areaElement.style.backgroundColor = 'rgba(' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ', 1)';

                    areaElement.appendChild(createElement('span', 'delete'));

                    idElement = createElement('span', 'id');
                    idElement.innerHTML = "#" + idCounter;
                    areaElement.appendChild(idElement);
                    areaElement.id = "area" + idCounter;

                    areaElement = null;
                    canvas.style.cursor = "default";
                    idCounter++;
                } else if(!deleteActive) {
                    mouse.startX = mouse.x;
                    mouse.startY = mouse.y;

                    areaElement = document.createElement('div');
                    areaElement.className = 'area';
                    areaElement.style.left = mouse.x + 'px';
                    areaElement.style.top = mouse.y + 'px';

                    canvas.appendChild(areaElement);
                    canvas.style.cursor = "crosshair";
                }
            };

            function createElement(tag, className) {
                var element = document.createElement(tag);
                element.className = className;

                if (className == 'delete') {
                    element.onclick = function () {
                        var areaToDelete = this.parentNode;
                        [].forEach.call(canvas.getElementsByClassName('area'), function(area) {
                            if (area == areaToDelete) {
                                canvas.removeChild(area);
                            }
                        });
                    };
                }
                return element;
            }
        }

        function drawImageOnCanvas() {
            ctx.clearRect(0, 0, image.width, image.height);
            var img = new Image();
            img.onload = function() {
                ctx.drawImage(img, 0, 0);
            };
            img.src = 'current.png?r=' + Math.random();
        }

        function getAreaAverageColor(x, y, w, h) {
            var pixelData = ctx.getImageData(parseInt(x), parseInt(y), parseInt(w), parseInt(h)).data;

            var rgb = {
                r: 0,
                g: 0,
                b: 0
            };

            var counter = 0;

            for (var i = 0; i < pixelData.length; i += 4) {
                rgb.r += pixelData[i];
                rgb.g += pixelData[i + 1];
                rgb.b += pixelData[i + 2];
                counter++;
            }

            rgb.r = Math.floor(rgb.r / counter);
            rgb.g = Math.floor(rgb.g / counter);
            rgb.b = Math.floor(rgb.b / counter);

            return rgb;
        }
    });
})(jQuery);