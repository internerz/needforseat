var path = require('path');

var imagecalibration = require('./routes/imagecalibration');
var client = require('./routes/client');
var prototype = require('./routes/prototype');

var PORT = 3000;
var express = require('express');
var app = express();
var http = require('http');
var server = http.createServer(app);

// view engine setup
app.set('views', __dirname + "/views");
app.set('view engine', 'jade');

app.use(require('node-compass')({mode: 'compressed'}));
app.use(express.static(__dirname + "/public"));

app.use('/', client);
app.use('/', imagecalibration);
app.use('/', prototype);

server.listen(PORT);

var socketio = require("socket.io");
var io = socketio.listen(server);

function Seat(id, occupation) {
    this.id = id;
    this.position = {
        x: 0,
        y: 0,
        w: 0,
        h: 0
    };
    this.color = {
        r: 0,
        g: 0,
        b: 0
    };

    this.occupation = occupation;

    this.setOccupation = function(occupation) {
        this.occupation = occupation;
    };

    this.setPosition = function(x, y, w, h) {
      this.position.x = x;
      this.position.y = y;
      this.position.w = w;
      this.position.h = h;
    };

    this.setColor = function (r, g, b) {
        this.color.r = r;
        this.color.g = g;
        this.color.b = b;
    }
}

var counter = 0;
var numberOfSeats = 4;
var seats = [];
var seatsSaved = false;
initSeats();

function initSeats() {
    for (var i = 0; i < numberOfSeats; i++) {
        seats.push(new Seat(i, false));
    }
}

io.sockets.on("connection", function(socket) {
    var isClient = false;

    socket.on("client", function(data) {
        isClient = !!(data.isClient);
    });

    socket.emit("seatsInit", seats);
    socket.emit("counterChange", counter);

    socket.on("seatsChanged", function(data) {
        var json = JSON.parse(data);

        var seatsToSend = [];

        for (var i = 0; i < json.s.length; i++) {
            seats[json.s[i].id].setOccupation(json.s[i].o == 1);
            seatsToSend.push(seats[json.s[i].id]);
        }

        socket.broadcast.emit("changeSeats", JSON.stringify(seatsToSend));
    });

    socket.on("seatsSaved", function(data) {
        var json = JSON.parse(data);

        [].forEach.call(json, function(seat) {
            seats[seat.id].setPosition(parseInt(seat.x), parseInt(seat.y), parseInt(seat.w), parseInt(seat.h));
            seats[seat.id].setColor(parseInt(seat.color.r), parseInt(seat.color.g), parseInt(seat.color.b));
        });

        seatsSaved = true;
        socket.broadcast.emit("seatDataSaved", seats);
        console.log("seat data saved");
    });

    socket.on("takePicture", function (data) {
        socket.broadcast.emit("takePicture", data);
    });

    socket.on("counterChange", function (count) {
        counter = count;
        socket.broadcast.emit("counterChange", counter);
    });

    socket.on("pictureTaken", function() {
       socket.broadcast.emit("pictureTaken");
    });
});